$('.login-btn').click(function(e) {  // отслеживаем нажате на кнопку submit
    e.preventDefault(); // отменяет действие браузера по умолчанию, т.е не даёт отправить данные в checkAuth
    let login = $('input[name="login"]').val().trim(); // получаем значения из формы, trim - удаляем лишние пробелы
    let password = $('input[name="password"]').val().trim();

    if (!login) {
        $('input[name="login"]').addClass("is-invalid");
        return false;
    } else if (!password) {
        $('input[name="password"]').addClass("is-invalid");
        return false;
    }

    $.ajax({
        url: 'app/checkAuth.php', // страница где обрабатываются данные
        type: 'POST',
        dataType: 'json',
        data: {             //Данные, которые будут отправлены на сервер.
            login: login,
            password: password,
        },
        beforeSend: function () {
           
        }, 
        success (response) { // данные, которые возращает checkAuth.php (здесь - текст ошибки)
            if (response.status) { // если без ошибок, то преходим на страницу с таблицей
                window.location.href = "app/table.php"
            }   else if (!response.status && (response.type===1) ){
                    $('input[name="login"]').addClass("is-invalid");
                }
                else if (!response.status && (response.type===2) ){
                    $('input[name="password"]').addClass("is-invalid");
                }
        }
    })
});
