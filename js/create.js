/* 
    Валидация для формы добавления в базу данных 
*/

$('.create-btn').click(function(e) {
    e.preventDefault(); 
   
    let name = $('input[name="name"]').val(); // получаем значения из формы, trim - удаляем лишние пробелы
    let parent = $('input[name="parent"]').val();
    let birthday = $('input[name="birthday"]').val();
    let num_group = $('input[name="num_group"]').val();
    let pass = $('input[name="pass"]').val();
    
    
    if ((name === '') ) {
        $('input[name="name"]').addClass("is-invalid");
        return false;
    } 
    if ((parent === '') ) {
        $('input[name="parent"]').addClass("is-invalid");
        return false;
    } 
    if (birthday === '') {
        $('input[name="birthday"]').addClass("is-invalid");
        return false;
        }
    if (num_group === '') {
        $('input[name="num_group"]').addClass("is-invalid");
        return false;
        }
    if (pass === '') {
        $('input[name="pass"]').addClass("is-invalid");
        return false;
        }

    $.ajax({
        url: 'create-general.php', // страница где обрабатываются данные
        type: 'POST',
        dataType: 'json',
        data: { //Данные, которые будут отправлены на сервер (у нас - на файл create.php).
            name: name,
            parent: parent,
            birthday: birthday,
            num_group: num_group,
            pass: pass
        },

        success () { // result - данные, которые возращает checkAuth.php (здесь - текст ошибки)

                document.location.href = "table.php"; 
             

        }
    })
});