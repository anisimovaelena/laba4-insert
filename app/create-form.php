<?php session_start();
if (!isset($_SESSION['user'])) { //если глобальная переменая session - пуста (авторизации не было) - то переход на форму авторизации
    header('Location: ../auth.php');
} ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/main.css">

</head>
<body>
<?php require_once('header.php'); 
require_once('connect.php');
?>
<div class="container">
<!-- Форма добавления -->
    <form >
            <div class="form-group row">
                <div class="col-5">
                <label for="name">Имя</label>
                <input type="text" class="form-control" name="name"> </div>
            </div>
            <div class="form-group row">
                <div class="col-5">
                    <label for="parent">Родитель</label>
                    <input class="form-control" type="name" name="parent"> </div>
                </div>
            <div class="form-group row">
            <div class="col-5">
                <label for="birthday">Дата рождения</label>
                <input class="form-control" type="date" name="birthday"> </div>
            </div>
            <div class="form-group row">
                <div class="col-5">
                <label for="num_group">Номер группы</label>
                <input class="form-control" type="number" name="num_group"> </div>
            </div>
            <div class="form-group row">
                <div class="col-5">
                <label for="pass">Кол-во пропусков в месяце</label>
                <input class="form-control" type="number" name="pass"> </div>
            </div>

            <div class="form-group row">
                <div class="col-5">
                    <button type="submit" class="create-btn btn btn-primary">Добавить</button>
                </div>
            </div>
            
        </form>
</div>
<?php require_once('footer.php');?>

<script src="../js/jquery-3.4.1.min.js"></script>
<script src="../js/create.js"></script>



</body>
</html>