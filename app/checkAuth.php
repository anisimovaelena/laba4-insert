<?php

session_start();
//require_once 'connect.php';

$error_fields = [];
$login = $_POST['login']; //получили из формы пароль и логин
$password = md5($_POST['password']);

$users = [
    ['login' => 'admin', 'password' => md5(123)],
    ['login' => 'moder', 'password' => md5(456)],
    ['login' => 'zam', 'password' => md5(789)],
];



if (!in_array($login, array_column($users, 'login'))) { //ищет $login в массиве users по строкам
    $response = [
        "status" => false,
        "type" => 1,
        "message" => "Неверный логин",
        "fields" => $error_fields
    ];
    echo json_encode($response);

    die();

} else {
    if (!in_array($password, array_column($users, 'password'))) { // проверка пароля
        $response = [
            "status" => false,
            "type" => 2,
            "message" => "Неверный пароль",
            "fields" => $error_fields
        ];
        echo json_encode($response); // чтобы преобразовать php массив в json
    }
    else  {
        $_SESSION['user'] = [
            "login" => $login ];

        $response = [
            "status" => true
        ];
        echo json_encode($response); // чтобы преобразовать php массив в json
        
    }
}
