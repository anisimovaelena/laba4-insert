<?php
session_start();
if (!isset($_SESSION['user'])) { //если глобальная переменая session - пуста (авторизации не было) - то переход на форму авторизации
    header('Location: ../auth.php');
}
require_once 'connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/main.css">

</head>
<body>


<?php require_once('header.php'); 

?>
<div class="container">
    <div class="row col-12">
        <table class="table thead-light">
            <tr >
            <th scope="col" >Номер</th>
            <th scope="col" >Имя</th>
            <th scope="col" >Родитель</th>
            <th scope="col" >Дата рождения</th>
            <th scope="col" >Номер группы</th>
            <th scope="col" >Кол-во пропусков в месяце</th>
            
            </tr>
            <?php

                    /* Делаем выборку всех строк из таблицы "general" */

                    $general = $link->prepare( "SELECT * FROM `general`");
                    $general->execute();

                    /*
                    * Перебираем массив и рендерим HTML с данными из массива
                    * Ключ 0 - id
                    * Ключ 1 - name
                    * Ключ 2 - parent
                    * Ключ 3 - birthday
                    * Ключ 4 - num_group
                    * Ключ 5 - pass
                    */

                    foreach ($general as $general) {
                        ?>
                            <tr>
                                <td scope="row" ><?= $general[0] ?></td>
                                <td><?= $general[1] ?></td>
                                <td><?= $general[2] ?></td>
                                <td><?= $general[3] ?></td>
                                <td><?= $general[4] ?></td>
                                <td><?= $general[5] ?></td>
                               
                            </tr>
                        <?php
                    } 
                ?>
        </table>
    </div>
    <div class="row">
            <a class="btn btn-info" href="create-form.php" role="button"> Добавить </a>
            
    </div>
</div>
<br>
<br>
    <div>
        <a class="btn btn-warning" href="logout.php"> Выход </a>
    </div>



<?php require_once('footer.php');?>

</body>
</html>