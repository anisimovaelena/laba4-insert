 <?php
    session_start(); 

    if (isset($_SESSION['user'])) {
      header('Location: app/table.php');
  }?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">

</head>
<body>
<?php require_once('app\header.php') ?>

        <div class="container">
            <div class="row">
                <form >
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input type="login" class="form-control" name="login" >
                        <div class="invalid-feedback text_login">
                          Неверный логин
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Пароль</label>
                      <input type="password" class="form-control" name="password" >
                      <div class="invalid-feedback text_password">
                          Неверный пароль
                        </div>
                    </div>
                    <button type="submit" class="login-btn btn btn-primary">Войти</button>
                </form>
            </div>
        </div>

   <?php require_once('app/footer.php') ?>

   <script src="js/jquery-3.4.1.min.js"></script>
   <script src="js/validate-auth.js"></script>

   </body>
</html>